### krc 
  
In this repo I'm going to store my solutions for exercises from the "C Programming Language" by Brian Kernighan and Dennis Ritchie.  
Here are some instructions for you if you want to compile and run anything you see here:
 
To **compile** specific exercise's source code use:  
``make <ch[X]ex[N]_[M].c>``  
where ``X`` - the chapter number  
``N``, ``M`` - exercise numbers  
e.g. ``make ch1ex1_4.c``  
(mind the `.c` in the target name)  
  
To **run** use:  
``make run ex=<ch[X]ex[N]-[M]>``   
(no `.c` in a binary name)
  
To **remove** all available **executables** run:  
``make clean``  
  
To see which ones are implemented use:  
``make progress``
  
Developed and tested on GNU Linux but I have in plans to make it possible to compile and run this stuff on Windows later.
 
