#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.12
// Print input one word per line

int main()
{
    int input;
    while ((input = getchar()) != EOF)
    {
        if (input != ' ' && input != '\t')
            putchar(input);
        else
            putchar('\n');
    } 
 
    return 0;
}
