// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.13
// Print histogram of the lengths of words in input

#include <stdio.h>
#define WORD_LENGTHS_AMOUNT 30

int main()
{
    int input;
    int word_length = 0;
    int insert_index = 0;
    int lengths[WORD_LENGTHS_AMOUNT] = {0};
    int lengths_frequency[WORD_LENGTHS_AMOUNT] = {0};

    while ((input = getchar()) != EOF && insert_index != WORD_LENGTHS_AMOUNT)
    {
        if (input != '\n' && input != '\t' && input != ' ')
            ++word_length;
        else 
        {
            int length_included = 0;
            int included_index;
            for (int i = 0; i < WORD_LENGTHS_AMOUNT; i++)
            {
                if (lengths[i] == word_length && word_length != 0)
                {
                    length_included = 1;
                    included_index = i;
                }    
            }

            if (length_included == 0)
            {
                lengths[insert_index] = word_length;
                ++lengths_frequency[insert_index];
                ++insert_index;
            }
            else if (length_included == 1)
                ++lengths_frequency[included_index];
             
            word_length = 0;
        }
    }

    printf("\n");
    for (int i = 0; i < insert_index; i++)
    {
        if (lengths[i] > 0)
        {
            printf("%3d: ", lengths[i]);
            for (int j = 0; j < lengths_frequency[i]; j++)
                printf("#");
            printf("\n");
        }
    }

    return 0;
}
