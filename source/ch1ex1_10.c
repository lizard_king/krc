#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.10
// Program to copy its input to its output, replacing each tab by \t, backspace by \b, backslash by \\.


int main()
{
    int input;
    while ((input = getchar()) != EOF)
    {
       if (input == '\n')
           printf("\\n\n");
       else if (input == '\t')
           printf("\\t");
       else if (input == '\\') 
           printf("\\\\");
       else
           printf("%c", input);
    } 
    
    return 0;
}
