#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.17
// Program to print all input lines that are longer than 80 characters

#define LENGTH_OF_LINE_TO_BE_PRINTED 80
#define MAX_LENGTH_OF_INPUT_LINE     800

int main()
{
    int input;
    int input_length = 0;
    int input_char_index = 0;
    char input_line[MAX_LENGTH_OF_INPUT_LINE] = {'\0'};

    while ((input = getchar()) != EOF && input_length <= MAX_LENGTH_OF_INPUT_LINE)
    {
        if (input != '\n')
        {
            ++input_length;
            input_line[input_char_index] = input;
            ++input_char_index;
        }
        else
        {
            if (input_length >= LENGTH_OF_LINE_TO_BE_PRINTED)
            {
                for (int i = 0; i < input_char_index; i++)
                    printf("%c", input_line[i]);
            }
            
            // Clear stored input_line
            for (int j = 0; j < input_char_index; j++)
                input_line[j] = '\0';
        }
    }
 
    return 0;
}

