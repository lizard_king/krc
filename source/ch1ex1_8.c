#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.8
// Count blanks, tabs and newlines

int main()
{
    int input;
    int blanks_amount, tabs_amount, newlines;
    blanks_amount = tabs_amount = newlines = 0; 
    
    printf("INPUT -> \n");
    while ((input = getchar()) != EOF)
    {
       if (input == ' ')
          ++blanks_amount;
       if (input == '\t')
          ++tabs_amount;
       if (input == '\n')
          ++newlines; 
    }

    printf("\nBlanks: %d\nTabs: %d\nNewlines: %d\n", blanks_amount, tabs_amount, newlines);
    
    return 0;
}
