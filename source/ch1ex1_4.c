#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.4
// Print Celsius to Fahrenheit table
// With heading (Exercise 1.3)

int main()
{
    printf("CELSIUS\tFAHRENHEIT\n");
    for (int celsius = 0; celsius <= 100; celsius+=5)
        printf("%d\t%6.2f\n", celsius, ((double) celsius * 9/5) + 32.0);
    
    return 0;
}
