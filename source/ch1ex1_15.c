#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.15
// Rewrite the temperature conversion program of Section 1.2 to use a function for conversion

double convert_celsius_to_fahrenheit(double celsius);


int main()
{
    printf("CELSIUS\tFAHRENHEIT\n");
    for (int celsius = 0; celsius <= 100; celsius+=5)
        printf("%d\t%6.2f\n", celsius, convert_celsius_to_fahrenheit((double) celsius));
    return 0;
}

double convert_celsius_to_fahrenheit(double celsius)
{
    return celsius * 9/5 + 32.0;
}
