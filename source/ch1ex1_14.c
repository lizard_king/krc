// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.14
// Print histogram of the frequencies of different characters in it's input

#include <stdio.h>
#define MAX_CHAR_AMOUNT 95 /* 95 printable ASCII characters */

int main()
{
    char inputed_characters[MAX_CHAR_AMOUNT];
    int inputed_characters_frequency[MAX_CHAR_AMOUNT] = {0};
    int input;
    int indx = 0;
    while ((input = getchar()) != EOF)
    {   
        int included = 0;
        for (int i = 0; i < indx; i++)
        {
            if (inputed_characters[i] == input)
                included = 1;
        }
        
        if (included == 0)
        {
            inputed_characters[indx] = input;
            ++inputed_characters_frequency[indx];
            indx++;    
        }
        else
        {
            for (int k = 0; k < MAX_CHAR_AMOUNT; k++)
            {
                if (inputed_characters[k] == input)
                    ++inputed_characters_frequency[k];
            }
        }
    }
   
    for (int d = 0; d < indx; d++)
    {
        if (inputed_characters[d] == '\n')
        {
            printf("NEWLINE: ");
            for (int _ = 0; _ < inputed_characters_frequency[d]; _++)
                printf("|");
            printf("\n");
        }
        else if (inputed_characters[d] == '\t')
        {
            printf("    TAB: ");
            for (int _ = 0; _ < inputed_characters_frequency[d]; _++)
                printf("|");
            printf("\n");
        }
        else if (inputed_characters[d] == ' ')
        {
            printf("  BLANK: ");
            for (int _ = 0; _ < inputed_characters_frequency[d]; _++)
                printf("|");
            printf("\n");
        }    
        else
        {
            printf("%7c: ", inputed_characters[d]);
            for (int _ = 0; _ < inputed_characters_frequency[d]; _++)
                printf("|");
           printf("\n"); 
        }
    }

    return 0;
}
