#include <stdio.h>

// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 2.10
// Rewrite the lower() function, which converts upper case letters to lowercase
// With a conditional expression instead of if-else.
// (added interactive input to play atound with the program)

int lower(int character);

int main()
{
    int input;
    while ((input = getchar()) != EOF)
        printf("%c", lower(input));
    printf("\n");

    return 0;
}

int lower(int character)
{
    return (character >= 'A' && character <= 'Z') ? character + 'a' - 'A' : character;
}

