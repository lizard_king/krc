// C Programming Language by Brian Kernighan and Dennis Ritchie.
// Exercise 1.19
// Program that reverses it's input one line at a time using reverse() function.

#include <stdio.h>

#define MAX_LEN 100

void reverse(char s[], char d[]);

int main()
{
    int input;
    char input_line[MAX_LEN];
    char reversed_input_line[MAX_LEN];
    int insert_index = 0;

    while ((input = getchar()) != EOF)
    {
        if (input != '\n')
        {
            input_line[insert_index] = input;
            insert_index++; 
        }
        else
        {
            input_line[insert_index] = '\0';
            reverse(input_line, reversed_input_line);
            
            if (insert_index != 0)
            {
                int k = 0;
                while (reversed_input_line[k] != '\0')
                {
                    printf("%c", reversed_input_line[k]);
                    ++k;
                }
                printf("\n");
            }
            insert_index = 0; 
            for (int i = 0; i < insert_index; i++)
            {
                input_line[i] = '\0';
                reversed_input_line[i] = '\0';
            }
        }
    }
    return 0;
}

void reverse(s, d)
char s[], d[];
{
    int len = 0;
    for (int i = 0; i < MAX_LEN; i++)
    {
        if (s[i] != '\0')
            ++len;
        else
            break;
    }
 
    for (int j = 0; j < len; j++)
        d[j] = s[len - j - 1];
}
