CC :=
CFLAGS :=
MVCMD :=
MKDIRS :=
SRC_PATH := source/
B_RM_MSG="Available build removed"
B_NOT_RM_MSG="No build available"
NTH_TO_RUN_MSG := "Nothing to run, perform 'make <excercise.c>' first"
EXT :=

ifeq ($(OS), Windows_NT)
	CC += cl
	CFLAGS += /nologo
	MKDIRS += mkdir -p build/bin; mkdir -p build/obj
	MVCMD += mv *.exe ./build/bin/; mv *.obj ./build/obj
	ext += exe
else
	CC += gcc
	CFLAGS += -Wall -o
	MKDIRS += mkdir -p build/bin
	MVCMD += mv *.out ./build/bin/
	EXT += out
endif

all:
	@printf "\n* To compile specific exercise's source code use:\nmake <ch[X]ex[N]_[M].c>\nwhere X - the chapter number\nN, M - exercise numbers\n\n"
	@printf "* To run use:\nmake run ex=<ch[X]ex[N]_[M]>\n\n* To see which ones are implemented use:\nmake progress\n\n" 
	@printf "* To remove all available executables run:\nmake clean\n"
%.c:
	@$(CC) $(SRC_PATH)$@ $(CFLAGS) $(basename $@).$(strip $(EXT))
	@$(MKDIRS)
	@$(MVCMD)
clean:
	@if [ -d "./build/" ]; then rm -rf ./build/; echo $(B_RM_MSG); else echo $(B_NOT_RM_MSG); fi
run:
	@if [ -f build/bin/$(ex).$(strip $(EXT)) ]; then build/bin/$(ex).$(strip $(EXT)); else echo $(NTH_TO_RUN_MSG); fi
progress: 
	@cat progress.txt | less

